{-|
Module: ServicoA
Description: Retreivel of exchange rates for Servico A
-}
{-# LANGUAGE DeriveGeneric #-}
module ServicoA (getRate) where

import Data.Text (Text)
import Data.Scientific (Scientific)
import Data.Aeson (FromJSON)

import GHC.Generics (Generic)

import Network.HTTP.Req

-- |Response body from ServicoA
data Cotacao = Cotacao { cotacao :: Scientific
                       , moeda :: Text
                       , symbol :: Text }
             deriving (Show, Eq, Generic)

instance FromJSON Cotacao

-- |Retreive the exchange rate for a currency on ServicoA.
-- Any errors will be reported as exceptions in the IO type
--
-- It takes a base Url + Options (as returned from urlQ) and the currency
-- for which to retrieve the exchange rate.
getRate :: (Url scheme, Option scheme) -> Text -> IO Scientific
getRate (baseUrl, baseOpts) currency = do
  let
    url = baseUrl /: "servico-a" /: "cotacao"
    options = baseOpts <> ("moeda" =: currency)

  resp <- runReq defaultHttpConfig $
    responseBody <$> req GET url NoReqBody jsonResponse options

  pure $ cotacao resp

