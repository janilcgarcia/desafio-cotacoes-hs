{-# LANGUAGE ScopedTypeVariables, QuasiQuotes, DeriveGeneric, DataKinds #-}
{-# LANGUAGE LambdaCase #-}
module Main where

import Web.Scotty

import Control.Exception (SomeException, catch)
import Control.Monad.IO.Class (liftIO)
import Control.Monad (void)
import Data.Scientific (Scientific)
import Control.Concurrent (threadDelay)
import Control.Concurrent.Async (async, mapConcurrently, race)
import Data.Text (Text, pack)
import Data.Function ((&))
import Control.Monad.Trans.Class (lift)
import Data.Maybe (mapMaybe)

import Data.Default (def)
import Network.Wai.Handler.Warp (setHost, setPort)

import Network.HTTP.Req (urlQ)
import qualified ServicoA
import qualified ServicoB

import GHC.Generics (Generic)

import Data.Aeson (ToJSON(..), genericToEncoding, defaultOptions, object, (.=))

import Network.HTTP.Types (serviceUnavailable503)

import qualified ServicoC as SC

data GetRateResult = TimeoutError
                   | OtherError SomeException
                   deriving Show

data Cotacao = Cotacao { cotacao :: Scientific
                       , moeda :: Text
                       , comparativo :: Text
                       , allResults :: [Text] }
             deriving (Show, Eq, Generic)

instance ToJSON Cotacao where
  toEncoding = genericToEncoding defaultOptions

app :: IO (ScottyM ())
app = do
  let callbackUrl = "http://172.27.146.25:9000/servico-c-callback"
  servicoCMVar <- SC.actionsMVar callbackUrl
  servicoCSTM <- SC.actionsSTM callbackUrl
  
  let
    timeout microsecs action = do
      let wait = do
            threadDelay microsecs
            pure TimeoutError

      race wait action `catch` \(e :: SomeException) ->
        pure $ Left (OtherError e)

    baseUrl = [urlQ|http://localhost:8080/|]

    prepareGetRate getRate = timeout 5000000 . getRate baseUrl

    getRates = prepareGetRate <$> [ ServicoA.getRate
                                  , ServicoB.getRate
                                  , SC.getRate servicoCMVar
                                  , SC.getRate servicoCSTM
                                  ]

  pure $ do
    get "/cotacoes/:moeda" $ do
      moeda <- param "moeda"

      rates <- lift $ mapConcurrently ($ moeda) getRates

      let validResults = mapMaybe (either (const Nothing) Just) rates

      case validResults of
        [] -> do
          status serviceUnavailable503
          let message = "No exchange rate found for this currency" :: Text
          json $ object ["error" .= message]
        results ->
          json $ Cotacao { cotacao = minimum results
                         , moeda = moeda
                         , comparativo = "BRL"
                         , allResults = pack . show <$> rates }

    post "/servico-c-callback" $ do
      callbackData <- jsonData
      -- Async and forget
      lift . async . timeout 1000000 $ SC.complete servicoCMVar callbackData
      lift . async . timeout 1000000 $ SC.complete servicoCSTM callbackData
      pure ()

main :: IO ()
main = do
  let
    updateSettings opts f = opts { settings = f $ settings opts }
    opts = updateSettings def $ setHost "0.0.0.0" . setPort 9000

  readyApp <- app

  scottyOpts opts readyApp
