{-|
Module: ServicoC.Callback
Description: Data definition of the callback request for ServioC

This module exists mainly for namespacing purposes: both the callback
request that ServicoC makes to us and the response body of the initiating
POST returning a field named "cid". Since Haskell records fields share
namespaces putting both on the same module would lead to name conflicts.

Another option would be using different names (like cidCallback for the 
callback and cidResponse for the response), but that requires a more complex
implementation of the FromJSON instance.

This is also nice because CotacaoCallback is actually used by both the routes
and the ServicoC module.
-}
{-# LANGUAGE DeriveGeneric #-}
module ServicoC.Callback where

import Data.Aeson (FromJSON)
import Data.Text (Text)
import Data.Scientific (Scientific)

import GHC.Generics (Generic)

-- |Callback body as it's send in the callback request
data CotacaoCallback = CotacaoCallback { cid :: Text
                                       , f :: Scientific
                                       , t :: Text
                                       , v :: Scientific }
                     deriving (Show, Eq, Generic)

instance FromJSON CotacaoCallback

-- |Get the exchange from the callback fields, since it's split in value
-- and factor
rate :: CotacaoCallback -> Scientific
rate CotacaoCallback { f = factor, v = value } = value / factor
