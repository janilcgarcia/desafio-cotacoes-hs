{-|
Module: ServicoA
Description: Retreivel of exchange rates for Servico A

This is the most interesting service, since it uses a URL callback to complete
the call. This module offers to different implementations for that one based
on MVar and another based on STM.
-}
{-# LANGUAGE DeriveGeneric, RankNTypes #-}
module ServicoC ( Actions(getRate, complete)
                , actionsMVar
                , actionsSTM
                ) where

import Control.Exception (bracket, bracket_)
import Control.Monad (when)

import Control.Concurrent.MVar
       ( newMVar
       , modifyMVar
       , newEmptyMVar
       , modifyMVar_
       , putMVar
       , readMVar )

import Control.Concurrent.STM
       ( newTVarIO
       , atomically
       , readTVar
       , writeTVar
       , retry
       , modifyTVar
       , putTMVar
       , readTMVar
       , newEmptyTMVarIO )

import Data.Text (Text)
import Data.Scientific (Scientific)
import Data.Aeson (ToJSON(..), FromJSON, genericToEncoding, defaultOptions)

import qualified Data.Map as M

import Network.HTTP.Req

import GHC.Generics (Generic)

import qualified ServicoC.Callback as C


-- |Body of the request for initiating the exchange rate retrieval
data RequestCotacao = RequestCotacao { tipo :: Text
                                     , callback :: Text }
                    deriving (Show, Eq, Generic)

-- |Body of the response of exchange retrieval initiation. The cid is used
-- for filtering out the callback later.
data RequestCotacaoResult = RequestCotacaoResult { mood :: Text
                                                 , cid :: Maybe Text
                                                 , mensagem :: Text
                                                 }
                          deriving (Show, Eq, Generic)

-- |Actions for ServicoC, it needs 2 action, not only getRate. complete is
-- used to complete a pending getRate.
data Actions =
  Actions { getRate :: forall scheme. (Url scheme, Option scheme)
                    -> Text
                    -> IO Scientific
          , complete :: C.CotacaoCallback -> IO () }

instance ToJSON RequestCotacao where
  toEncoding = genericToEncoding defaultOptions

instance FromJSON RequestCotacaoResult

-- |Initialiate the exchange retrieval
-- The first param is callback url, the second the currency to which fetch
-- the exchange rate, the third the baseUrl and options for the service.
makeRequest :: Text -> Text -> (Url scheme, Option scheme) -> IO RequestCotacaoResult
makeRequest callbackUrl currency (baseUrl, options) = do
  let url = baseUrl /: "servico-c" /: "cotacao"
      body = RequestCotacao { tipo = currency
                            , callback = callbackUrl }

  runReq defaultHttpConfig $
    responseBody <$> req POST url (ReqBodyJson body) jsonResponse options

-- |Create actions based on MVar
actionsMVar :: Text -> IO Actions
actionsMVar callbackUrl = do
  -- Create a new MVar, this is a variable, always full
  callbackMapM <- newMVar M.empty

  -- Setup creates an MVar that holds an MVar and puts it into the map
  -- of callbacks with the appropriate id. The complete will read the value
  -- of the outer MVar and will put into the inner MVar, getRate will put
  -- an empty MVar into the outer MVar and read from the inner MVar.

  -- That way if getRate runs first for a certain cid it will wait for
  -- complete to fill the MVar
  -- If complete runs first, it will wait for getRate to put a "promise" that
  -- it can fill
  -- so both actions wait on one another
  let setup id = modifyMVar callbackMapM $ \map -> do
        case M.lookup id map of
          Nothing -> do
            latch <- newEmptyMVar
            pure (M.insert id latch map, latch)

          Just latch -> pure (map, latch)

  -- cleanup removes id from the map, so memory don't leak
  let cleanup id = modifyMVar_ callbackMapM $ pure . M.delete id

  let waitCallback id = bracket (setup id) (const $ cleanup id) $ \latch -> do
        promise <- newEmptyMVar
        putMVar latch promise

        readMVar promise

  let fillCallback id rate =
        bracket (setup id) (const $ cleanup id) $ \latch -> do
          promise <- readMVar latch
          putMVar promise rate

  let getRate baseUrl currency = do
        result <- makeRequest callbackUrl currency baseUrl

        case cid result of
          Nothing -> fail "Invalid currency"
          Just cid ->
            waitCallback cid

  let complete callback = do
        fillCallback (C.cid callback) (C.rate callback)

  pure $ Actions { getRate = getRate
                 , complete = complete }

-- |Create actions based on STM
actionsSTM :: Text -> IO Actions
actionsSTM callbackUrl = do
  -- The STM implementation uses a Map CID (TMVar Scientific)
  -- The TMVar is used for waiting completion
  callbackMap <- newTVarIO M.empty

  -- setup and clean only need to run on getRate, so resource management is
  -- local here
  
  -- setup create an empty TMVar and stores it in the callbackMap
  let setup id = do
        promise <- newEmptyTMVarIO
        atomically $ modifyTVar callbackMap $ M.insert id promise
        pure promise

  let clean id = atomically . modifyTVar callbackMap $ M.delete id

  -- just reads the rate from the TMVar
  let readRate promise = atomically $ readTMVar promise

  let getRate baseUrl currency = do
        result <- makeRequest callbackUrl currency baseUrl

        case cid result of
          Nothing -> fail "Invalid currency"
          Just id -> bracket (setup id) (const $ clean id) readRate

  -- complete actually waits for when the CID key is present on the callback
  -- map, when it finally is, it just fills the TMVar associated with it
  let complete callback = atomically $ do
        map <- readTVar callbackMap
    
        case M.lookup (C.cid callback) map of
          Nothing -> retry
          Just promise -> putTMVar promise $ C.rate callback


  pure $ Actions { getRate = getRate
                 , complete = complete }