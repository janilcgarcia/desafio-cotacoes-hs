{-|
Module: ServicoB
Description: Retrieval of exchange rates for Servico B
-}
{-# LANGUAGE DeriveGeneric #-}
module ServicoB (getRate) where

import Data.Text (Text, unpack)
import Data.Scientific (Scientific)

import Data.Aeson ( FromJSON(..)
                  , withObject
                  , genericParseJSON
                  , defaultOptions
                  )
import Data.Aeson.Types (parseFail)

import qualified Data.Aeson.KeyMap as KM

import GHC.Generics (Generic)

import Network.HTTP.Req

data Cotacao = Cotacao { fator :: Int
                       , currency :: Text
                       , valor :: Text }
             deriving (Show, Eq, Generic)

instance FromJSON Cotacao where
  parseJSON = withObject "Cotacao" $ \obj ->
    case KM.lookup "cotacao" obj of
      Nothing -> parseFail "Object doesn't contain cotacao field"
      Just value -> genericParseJSON defaultOptions value

-- |Retreive the exchange rate for a currency on ServicoA.
-- Same interface as ServicoA.getRate
getRate :: (Url scheme, Option scheme) -> Text -> IO Scientific
getRate (baseUrl, baseOpts) currency = do
  let
    url = baseUrl /: "servico-b" /: "cotacao"
    options = baseOpts <> "curr" =: currency

  resp <- runReq defaultHttpConfig $
    responseBody <$> req GET url NoReqBody jsonResponse options

  pure $ read (unpack $ valor resp) / fromIntegral (fator resp)

